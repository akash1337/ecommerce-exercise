<%@ page import="java.util.List" %>
<%@ page import="com.example.ecommerce.pagination.Page" %>
<%@ page import="com.example.ecommerce.services.CategoryService" %>
<%@ page import="com.example.ecommerce.models.Category" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf"
            crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"
          integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w=="
          crossorigin="anonymous"/>
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">

    <title>Categories</title>
</head>
<body>
<div class="container">
    <nav class="navbar navbar-expand-lg navbar-light bg-light mb-5">
        <div class="container-fluid">
            <a class="navbar-brand" href="/">Ecommerce</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup"
                    aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div class="navbar-nav">
                    <a class="nav-link active" aria-current="page" href="/">Home</a>
                    <a class="nav-link" href="products.jsp">Products</a>
                    <a class="nav-link" href="categories.jsp">Categories</a>
                </div>
            </div>
        </div>
    </nav>
    <%
        CategoryService categoryService = new CategoryService();
        int currPageNum = 1;
        try {
            currPageNum = Integer.parseInt(request.getParameter("page"));
        } catch (NumberFormatException e) {
        }
        Page<Category> currPage = categoryService.getPage(currPageNum, 2);
        List<Category> categories = currPage.getRecords();
    %>
    <%
        Boolean showAlert = request.getParameter("msg") != null;
        String alertClass = "info";
        if (request.getParameter("success") != null && request.getParameter("success").equals("1"))
            alertClass = "success";
        else if (request.getParameter("error") != null && request.getParameter("error").equals("1"))
            alertClass = "danger";
    %>

    <% if (showAlert) { %>
    <div class="alert alert-<%= alertClass %>" role="alert">
        <%= request.getParameter("msg") %>
    </div>
    <% } %>

    <%
        String action = "/ServletAddCategory";
        int id = 0;
        String name = "";
        if (request.getParameter("action") != null && request.getParameter("action").equals("edit") && request.getParameter("categoryID") != null) {
            try {
                Category c = categoryService.get(Integer.parseInt(request.getParameter("categoryID")));
                id = c.getId();
                name = c.getName();
                action = "/ServletEditCategory";
            } catch (NumberFormatException e) {
            }
        }
    %>
    <form action="<%= action %>" method="post">
        <div class="row g-3 align-items-center">
            <input type="hidden" name="categoryID" value="<%= id %>">
            <div class="col-auto">
                <label for="categoryName" class="col-form-label">Name</label>
            </div>
            <div class="col-auto">
                <input type="text" id="categoryName" name="categoryName" class="form-control" value="<%= name %>">
            </div>
            <div class="col-auto">
                <button type="submit" class="btn btn-primary"><%= action == "/ServletAddCategory" ? "Add" : "Update" %>
                </button>
            </div>
        </div>
    </form>
    <table class="table">
        <thead>
        <tr class="d-flex">
            <th class="col-1" scope="col">#</th>
            <th class="col-3" scope="col">Name</th>
            <th class="col-2" scope="col">Action</th>
        </tr>
        </thead>
        <tbody>
        <% for (Category category : categories) { %>
        <tr class="d-flex">
            <td class="col-1" scope="row">
                <%= category.getId() %>
            </td>
            <td class="col-3">
                <%= category.getName() %>
            </td>
            <td class="col-2">
                <div class="row">
                    <div class="col-4">
                        <form action="" method="post">
                            <input type="hidden" name="action" value="edit"/>
                            <input type="hidden" name="categoryID" value="<%= category.getId() %>"/>
                            <button type="submit" class="btn btn-primary"><i class="far fa-edit"></i></button>
                        </form>
                    </div>
                    <div class="col-4">
                        <form action="/ServletDeleteCategory" method="post">
                            <input type="hidden" name="categoryID" value="<%= category.getId() %>"/>
                            <button type="submit" class="btn btn-danger"><i class="fas fa-trash"></i></button>
                        </form>
                    </div>
                </div>
            </td>
        </tr>
        <% } %>
        </tbody>
    </table>
    <nav aria-label="Page navigation">
        <ul class="pagination float-right">
            <li class="page-item <%= currPageNum != 1 ? "": "disabled" %>">
                <a class="page-link" href="?page=<%= currPageNum - 1 %>">Previous</a>
            </li>
            <%
                int totalPages = currPage.getTotalPages();
                for (int i = 0; i < totalPages; i++) {
            %>
            <li class="page-item <%= currPageNum == i+1 ? "active": "" %>">
                <a class="page-link" href="?page=<%=i+1%>"><%=i + 1%>
                </a>
            </li>
            <%}%>
            <li class="page-item <%= currPageNum != totalPages ? "": "disabled" %>">
                <a class="page-link" href="?page=<%=currPageNum + 1 %>">Next</a>
            </li>
        </ul>
    </nav>

</div>
</body>
</html>
