<%@ page import="com.example.ecommerce.services.ProductService" %>
<%@ page import="java.util.List" %>
<%@ page import="com.example.ecommerce.models.Product" %>
<%@ page import="com.example.ecommerce.pagination.Page" %>
<%@ page import="com.example.ecommerce.services.CategoryService" %>
<%@ page import="com.example.ecommerce.models.Category" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="java.util.ArrayList" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf"
            crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"
          integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w=="
          crossorigin="anonymous"/>
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <title>Products</title>
</head>
<body>
<div class="container">
    <nav class="navbar navbar-expand-lg navbar-light bg-light mb-5">
        <div class="container-fluid">
            <a class="navbar-brand" href="/">Ecommerce</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup"
                    aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div class="navbar-nav">
                    <a class="nav-link active" aria-current="page" href="/">Home</a>
                    <a class="nav-link" href="products.jsp">Products</a>
                    <a class="nav-link" href="categories.jsp">Categories</a>
                </div>
            </div>
        </div>
    </nav>
    <%
        ProductService productService = new ProductService();
        int currPageNum = 1;
        try {
            currPageNum = Integer.parseInt(request.getParameter("page"));
        } catch (NumberFormatException e) {
        }
        Page<Product> currPage = productService.getPage(currPageNum, 2);
        List<Product> products = currPage.getRecords();

        CategoryService categoryService = new CategoryService();
        List<Category> categories = categoryService.getAll();

    %>

    <%
        Boolean showAlert = request.getParameter("msg") != null;
        String alertClass = "info";
        if (request.getParameter("success") != null && request.getParameter("success").equals("1"))
            alertClass = "success";
        else if (request.getParameter("error") != null && request.getParameter("error").equals("1"))
            alertClass = "danger";
    %>

    <% if (showAlert) { %>
    <div class="alert alert-<%= alertClass %>" role="alert">
        <%= request.getParameter("msg") %>
    </div>
    <% } %>

    <div class="row align-items-start">
        <div class="col-8">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Categories</th>
                    <th scope="col">Mfg Date</th>
                    <th scope="col">Exp Date</th>
                    <th scope="col">Action</th>
                </tr>
                </thead>
                <tbody>
                <% for (Product product : products) { %>
                <tr>
                    <td scope="row">
                        <%= product.getId() %>
                    </td>
                    <td>
                        <%= product.getName() %>
                    </td>
                    <td>
                        <% categoryService.fetchCategoriesByProduct(product); %>
                        <%= product.getCategories().stream().map(c -> c.getName()).collect(Collectors.joining("<br/>")) %>
                    </td>
                    <td>
                        <%= product.getMfgDate().toString() %>
                    </td>
                    <td>
                        <%= product.getExpDate().toString() %>
                    </td>
                    <td>
                        <div class="row">
                            <div class="col-sm-4">
                                <form action="" method="post">
                                    <input type="hidden" name="action" value="edit"/>
                                    <input type="hidden" name="productID" value="<%= product.getId() %>"/>
                                    <button type="submit" class="btn btn-primary"><i class="far fa-edit"></i></button>
                                </form>
                            </div>
                            <div class="col-sm-4">
                                <form action="ServletDeleteProduct" method="post">
                                    <input type="hidden" name="productID" value="<%= product.getId() %>"/>
                                    <button type="submit" class="btn btn-danger"><i class="fas fa-trash"></i></button>
                                </form>
                            </div>
                        </div>
                    </td>
                </tr>
                <% } %>
                </tbody>
            </table>
            <nav aria-label="Page navigation">
                <ul class="pagination float-right">
                    <li class="page-item <%= currPageNum != 1 ? "": "disabled" %>">
                        <a class="page-link" href="?page=<%= currPageNum - 1 %>">Previous</a>
                    </li>
                    <%
                        int totalPages = currPage.getTotalPages();
                        for (int i = 0; i < totalPages; i++) {
                    %>
                    <li class="page-item <%= currPageNum == i+1 ? "active": "" %>">
                        <a class="page-link" href="?page=<%=i+1%>"><%=i + 1%>
                        </a>
                    </li>
                    <%}%>
                    <li class="page-item <%= currPageNum != totalPages ? "": "disabled" %>">
                        <a class="page-link" href="?page=<%=currPageNum + 1 %>">Next</a>
                    </li>
                </ul>
            </nav>
        </div>
        <div class="col-4">
            <%
                String action = "/ServletAddProduct";
                int id = 0;
                String name = "";
                String mfgDate = "";
                String expDate = "";
                List<Integer> selectedCat = new ArrayList<>();
                if (request.getParameter("action") != null && request.getParameter("action").equals("edit") && request.getParameter("productID") != null) {
                    try {
                        Product p = productService.get(Integer.parseInt(request.getParameter("productID")));
                        id = p.getId();
                        name = p.getName();
                        mfgDate = p.getMfgDate().toString();
                        expDate = p.getExpDate().toString();
                        categoryService.fetchCategoriesByProduct(p);
                        selectedCat = p.getCategories().stream().map(c -> c.getId()).collect(Collectors.toList());
                        action = "/ServletEditProduct";
                    } catch (NumberFormatException e) {
                    }
                }
            %>
            <form action="<%= action %>" method="post">
                <input type="hidden" name="productID" value="<%= id %>">
                <div class="mb-3">
                    <label for="productName" class="form-label">Name</label>
                    <input type="text" id="productName" value="<%= name %>" name="productName" class="form-control"
                           required>
                </div>
                <div class="mb-3">
                    <label for="productMfgDate" class="col-form-label">MFG Date</label>
                    <input type="date" value="<%= mfgDate %>" id="productMfgDate" name="productMfgDate"
                           class="form-control"
                           required>
                </div>
                <div class="mb-3">
                    <label for="productExpDate" class="col-form-label">Exp Date</label>
                    <input type="date" value="<%= expDate %>" id="productExpDate" name="productExpDate"
                           class="form-control"
                           required>
                </div>
                <div class="mb-3">
                    <label for="productCategories" class="col-form-label">Categories</label>
                    <select id="productCategories" name="productCategories" size="3" class="form-select" multiple>
                        <option value="-1" <%= selectedCat.size() == 0 ? "selected" : "" %>>No Category</option>
                        <% for (Category c : categories) {%>
                        <option <%= selectedCat.contains(c.getId()) ? "selected" : "" %>
                                value="<%= c.getId() %>"><%= c.getName() %>
                        </option>
                        <% } %>
                    </select>
                </div>
                <button type="submit" class="btn btn-primary"><%= action == "/ServletAddProduct" ? "Add" : "Update" %>
                </button>
            </form>
        </div>
    </div>
</div>
</body>
</html>
