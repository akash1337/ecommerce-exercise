package com.example.ecommerce.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBService {
    private static final String url = "jdbc:mysql://localhost:3306/";
    private static final String db = "ecommerce";
    private static final String userName = "root";
    private static final String password = "root";
    protected static Connection conn;

    public static Connection createConnection() throws SQLException, ClassNotFoundException {
        Class.forName("com.mysql.cj.jdbc.Driver");
        return DriverManager.getConnection(url + db, userName, password);
    }

    public static Connection getConnection() {
        if (conn == null) {
            try {
                return createConnection();
            } catch (SQLException | ClassNotFoundException e) {
                e.printStackTrace();
                throw new RuntimeException("Database connection error");
            }
        }
        return conn;
    }

}
