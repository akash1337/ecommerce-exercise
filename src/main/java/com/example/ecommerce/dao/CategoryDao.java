package com.example.ecommerce.dao;

import com.example.ecommerce.db.DBService;
import com.example.ecommerce.models.Category;
import com.example.ecommerce.models.Product;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


public class CategoryDao implements Dao<Category> {
    private Connection conn;

    public CategoryDao() {
        this.conn = DBService.getConnection();
    }

    @Override
    public Category get(int id) {
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT id, name FROM categories WHERE id=" + id);
            while (rs.next()) {
                Category c = new Category(rs.getString(2));
                c.setId(rs.getInt(1));
                return c;
            }
        } catch (SQLException e) {
            System.out.println("SQL Error while getting category. Category ID " + id + ". Error: " + e.getMessage());
        }
        return null;
    }

    public int count() {
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT COUNT(id) FROM categories");
            if (rs.next())
                return rs.getInt(1);
        } catch (SQLException e) {
            System.out.println("SQL Error while getting count of products. Error: " + e.getMessage());
        }
        return 0;
    }

    public List<Category> getAll(int offset, int limit) {
        List<Category> result = new ArrayList<>();
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(String
                    .format("SELECT id, name FROM categories LIMIT %d OFFSET %d", limit, offset));
            while (rs.next()) {
                Category c = new Category(rs.getString(2));
                c.setId(rs.getInt(1));
                result.add(c);
            }
        } catch (SQLException e) {
            System.out.println("SQL Error while getting products list. Error: " + e.getMessage());
        }
        return result;
    }

    @Override
    public List<Category> getAll() {
        List<Category> result = new ArrayList<>();
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT id, name FROM categories");
            while (rs.next()) {
                Category c = new Category(rs.getString(2));
                c.setId(rs.getInt(1));
                result.add(c);
            }
        } catch (SQLException e) {
            System.out.println("SQL Error while getting categories list. Error: " + e.getMessage());
        }
        return result;
    }

    public void fetchCategoriesByProduct(Product p) {
        List<Category> result = new ArrayList<>();
        try {
            String query = String.join(" ", "SELECT c.id, c.name FROM categories c", "inner join product_category on", "product_category.categoryId=c.id and product_category.productID=?");
            PreparedStatement pst = conn.prepareStatement(query);
            pst.setInt(1, p.getId());
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                Category c = new Category(rs.getString(2));
                c.setId(rs.getInt(1));
                result.add(c);
            }
        } catch (SQLException e) {
            System.out.println("SQL Error while getting products list by category. Error: " + e.getMessage());
        }
        p.setCategories(result);
    }

    @Override
    public void save(Category c) {
        if (c == null)
            return;
        try {
            PreparedStatement pst = conn.prepareStatement("INSERT INTO categories (name) VALUES(?)");
            pst.setString(1, c.getName());
            pst.executeUpdate();
        } catch (SQLException e) {
            System.out.println("SQL Error while inserting category, Category ID " + c.getId());
        }
    }

    @Override
    public void update(Category c) {
        if (c == null)
            return;
        try {
            PreparedStatement pst = conn.prepareStatement("UPDATE categories SET name=? WHERE id=?");
            pst.setString(1, c.getName());
            pst.setInt(2, c.getId());
            pst.executeUpdate();
        } catch (SQLException e) {
            System.out.println("SQL Error while updating category, Category ID " + c.getId());
        }
    }

    @Override
    public void delete(Category c) {
        if (c == null)
            return;
        try {
            Statement st = conn.createStatement();
            st.executeUpdate("DELETE FROM product_category where categoryId=" + c.getId());
            st.executeUpdate("DELETE FROM categories where id=" + c.getId());
        } catch (SQLException e) {
            System.out.println(
                    "SQL Error while deleting category, Category ID " + c.getId() + ". Error: " + e.getMessage());
        }
    }
}
