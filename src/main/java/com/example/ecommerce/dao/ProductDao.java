package com.example.ecommerce.dao;

import com.example.ecommerce.db.DBService;
import com.example.ecommerce.models.Category;
import com.example.ecommerce.models.Product;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class ProductDao implements Dao<Product> {
    private Connection conn;

    public ProductDao() {
        this.conn = DBService.getConnection();
    }

    @Override
    public Product get(int id) {
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT id, name, mfg_date, exp_date FROM products WHERE id=" + id);
            while (rs.next()) {
                Product p = new Product(rs.getString(2));
                p.setId(rs.getInt(1));
                p.setMfgDate(rs.getDate(3));
                p.setExpDate(rs.getDate(4));
                return p;
            }
        } catch (SQLException e) {
            System.out.println("SQL Error while getting product. Product ID " + id + ". Error: " + e.getMessage());
        }
        return null;
    }

    public List<Product> getAllByCategory(Category c) {
        List<Product> result = new ArrayList<>();
        try {
            Statement st = conn.createStatement();
            String query = String.join(" ", "SELECT p.id, p.name, p.mfg_date, p.exp_date FROM products p",
                    "inner join product_category on", "p.id=product_category.productId inner join",
                    "categories on categories.id=product_category.categoryId");
            ResultSet rs = st.executeQuery(query);
            iterateResult(result, rs);
        } catch (SQLException e) {
            System.out.println("SQL Error while getting products list by category. Error: " + e.getMessage());
        }
        return result;
    }

    public int count() {
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT COUNT(id) FROM products");
            if (rs.next())
                return rs.getInt(1);
        } catch (SQLException e) {
            System.out.println("SQL Error while getting count of products. Error: " + e.getMessage());
        }
        return 0;
    }

    public List<Product> getAll(int offset, int limit) {
        List<Product> result = new ArrayList<>();
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(String
                    .format("SELECT id, name, mfg_date, exp_date FROM products LIMIT %d OFFSET %d", limit, offset));
            iterateResult(result, rs);
        } catch (SQLException e) {
            System.out.println("SQL Error while getting products list. Error: " + e.getMessage());
        }
        return result;
    }

    private void iterateResult(List<Product> result, ResultSet rs) throws SQLException {
        while (rs.next()) {
            Product p = new Product(rs.getString(2));
            p.setId(rs.getInt(1));
            p.setMfgDate((java.util.Date) rs.getDate(3));
            p.setExpDate(rs.getDate(4));
            result.add(p);
        }
    }

    @Override
    public List<Product> getAll() {
        List<Product> result = new ArrayList<>();
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT id, name, mfg_date, exp_date FROM products");
            iterateResult(result, rs);
        } catch (SQLException e) {
            System.out.println("SQL Error while getting products list. Error: " + e.getMessage());
        }
        return result;
    }

    public void unlinkProduct(Product p) {
        if (p == null)
            return;
        try {
            PreparedStatement pst = conn.prepareStatement("DELETE FROM product_category WHERE productId=?");
            pst.setInt(1, p.getId());
            pst.executeUpdate();
        } catch (SQLException e) {
            System.out.println(String.format("SQL Error while unlinking product. Product ID %d. Error: %s", p.getId(), e.getMessage()));
        }
    }

    public void linkProduct(Category c, Product p) {
        if (c == null || p == null)
            return;
        try {
            PreparedStatement pst = conn.prepareStatement("INSERT INTO product_category (productId, categoryId) VALUES(?,?)");
            pst.setInt(1, p.getId());
            pst.setInt(2, c.getId());
            pst.executeUpdate();
        } catch (SQLException e) {
            System.out.println(String.format("SQL Error while linking product. Product ID %d Category ID %d. Error: %s", p.getId(), c.getId(), e.getMessage()));
        }
    }

    @Override
    public void save(Product p) {
        if (p == null)
            return;
        try {
            PreparedStatement pst = conn.prepareStatement("INSERT INTO products (name, mfg_date, exp_date) VALUES(?,?,?)", Statement.RETURN_GENERATED_KEYS);
            pst.setString(1, p.getName());
            pst.setDate(2, new Date(p.getMfgDate().getTime()));
            pst.setDate(3, new Date(p.getExpDate().getTime()));
            pst.executeUpdate();
            ResultSet rs = pst.getGeneratedKeys();
            if (rs.next()) {
                p.setId(rs.getInt(1));
                p.getCategories().stream().filter(c -> c.getId() != -1).forEach(c -> this.linkProduct(c, p));
            }
        } catch (SQLException e) {
            System.out.printf("SQL Error while inserting product, Product ID %d. Error: %s\n", p.getId(), e.getMessage());
        }
    }

    @Override
    public void update(Product p) {
        if (p == null)
            return;
        try {
            PreparedStatement pst = conn.prepareStatement("UPDATE products SET name=?, mfg_date=?, exp_date=? WHERE id=?");
            pst.setString(1, p.getName());
            pst.setDate(2, new Date(p.getMfgDate().getTime()));
            pst.setDate(3, new Date(p.getExpDate().getTime()));
            pst.setInt(4, p.getId());
            pst.executeUpdate();
            this.unlinkProduct(p);
            p.getCategories().stream().filter(c -> c.getId() != -1).forEach(c -> this.linkProduct(c, p));
        } catch (SQLException e) {
            System.out.printf("SQL Error while updating product, Product ID %d. Error: %s\n", p.getId(), e.getMessage());
        }
    }

    @Override
    public void delete(Product p) {
        if (p == null)
            return;
        try {
            this.unlinkProduct(p);
            Statement st = conn.createStatement();
            st.executeUpdate("DELETE FROM product_category where productId=" + p.getId());
            st.executeUpdate("DELETE FROM products where id=" + p.getId());
        } catch (SQLException e) {
            System.out.println(
                    "SQL Error while deleting product, Product ID " + p.getId() + ". Error: " + e.getMessage());
        }
    }
}
