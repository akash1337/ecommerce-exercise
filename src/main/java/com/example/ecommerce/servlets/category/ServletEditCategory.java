package com.example.ecommerce.servlets.category;

import com.example.ecommerce.models.Category;
import com.example.ecommerce.services.CategoryService;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.stream.Stream;

@WebServlet(name = "ServletEditCategory", value = "/ServletEditCategory")
public class ServletEditCategory extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (Stream.of("categoryID","categoryName").anyMatch(e -> request.getParameter(e) == null)) {
            response.sendRedirect("categories.jsp?error=1&msg=Invalid Parameters");
            return;
        }

        CategoryService categoryService = new CategoryService();
        Category category = new Category(request.getParameter("categoryName"));
        try {
            category.setId(Integer.parseInt(request.getParameter("categoryID")));
            categoryService.update(category);
        } catch (NumberFormatException e) {
            response.sendRedirect("categories.jsp?error=1&msg=Invalid CategoryID");
            return;
        }
        response.sendRedirect("categories.jsp?msg=Category Updated&success=1");
    }
}
