package com.example.ecommerce.servlets.category;

import com.example.ecommerce.models.Category;
import com.example.ecommerce.services.CategoryService;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.stream.Stream;

@WebServlet(name = "ServletAddCategory", value = "/ServletAddCategory")
public class ServletAddCategory extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (request.getParameter("categoryName") == null) {
            response.sendRedirect("categories.jsp?error=1&msg=Invalid Parameters");
            return;
        }

        CategoryService categoryService = new CategoryService();
        Category category = new Category(request.getParameter("categoryName"));
        categoryService.save(category);
        response.sendRedirect("categories.jsp?success=1&msg=Category Added");
    }
}
