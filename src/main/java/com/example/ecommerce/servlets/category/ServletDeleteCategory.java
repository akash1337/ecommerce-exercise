package com.example.ecommerce.servlets.category;

import com.example.ecommerce.models.Category;
import com.example.ecommerce.services.CategoryService;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

@WebServlet(name = "ServletDeleteCategory", value = "/ServletDeleteCategory")
public class ServletDeleteCategory extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (request.getParameter("categoryID") == null) {
            response.sendRedirect("categories.jsp?error=1&msg=Invalid Category ID");
            return;
        }

        CategoryService categoryService = new CategoryService();
        Category category = new Category("");
        try {
            category.setId(Integer.parseInt(request.getParameter("categoryID")));
        } catch (NumberFormatException e) {
            response.sendRedirect("categories.jsp?error=1&msg=Invalid Category ID");
            return;
        }
        categoryService.delete(category);
        response.sendRedirect("categories.jsp?success=1&msg=Category Deleted");
    }
}
