package com.example.ecommerce.servlets.product;

import com.example.ecommerce.models.Category;
import com.example.ecommerce.models.Product;
import com.example.ecommerce.services.ProductService;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@WebServlet(name = "ServletAddProduct", value = "/ServletAddProduct")
public class ServletAddProduct extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (Stream.of("productName", "productMfgDate", "productExpDate").anyMatch(e -> request.getParameter(e) == null)) {
            response.sendRedirect("products.jsp?error=1&msg=Invalid Parameters");
            return;
        }


        ProductService productService = new ProductService();
        Product product = new Product(request.getParameter("productName"));
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            product.setMfgDate(dateFormat.parse(request.getParameter("productMfgDate")));
            product.setExpDate(dateFormat.parse(request.getParameter("productExpDate")));
            if (request.getParameterValues("productCategories") != null) {
                product.setCategories(Arrays.stream(request.getParameterValues("productCategories")).map(c -> {
                    Category x = new Category("");
                    try {
                        x.setId(Integer.parseInt(c));
                    } catch (NumberFormatException e) {
                        x.setId(-1);
                        System.out.println("Can't parse categoryID" + c);
                    }
                    return x;
                }).collect(Collectors.toList()));
            }
            productService.save(product);
            response.sendRedirect("products.jsp?msg=Product Added&success=1");
        } catch (Exception e) {
            response.sendRedirect("products.jsp?msg=" + e.getMessage() + "&error=1");
        }
    }
}
