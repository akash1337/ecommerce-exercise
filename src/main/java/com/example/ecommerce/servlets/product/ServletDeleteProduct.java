package com.example.ecommerce.servlets.product;

import com.example.ecommerce.models.Product;
import com.example.ecommerce.services.ProductService;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

@WebServlet(name = "ServletDeleteProduct", value = "/ServletDeleteProduct")
public class ServletDeleteProduct extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getParameter("productID") == null) {
            resp.sendRedirect("products.jsp?error=1&msg=Invalid Product ID");
            return;
        }

        ProductService productService = new ProductService();
        Product product = new Product("");
        try {
            product.setId(Integer.parseInt(req.getParameter("productID")));
        } catch (NumberFormatException e) {
            resp.sendRedirect("products.jsp?error=1&msg=Invalid Product ID");
            return;
        }
        productService.delete(product);
        resp.sendRedirect("products.jsp?success=1&msg=Product Deleted");
    }
}
