package com.example.ecommerce.pagination;

import java.util.List;

public class Page<T> {
    private int pageNo;
    private int size;
    private int total;
    private List<T> records;

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public void setRecords(List<T> records) {
        this.records = records;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getPageNo() {
        return pageNo;
    }

    public int getSize() {
        return size;
    }

    public List<T> getRecords() {
        return records;
    }

    public int getTotalPages() {
        int totalPages = total / size;
        return total % size == 0 ? totalPages : totalPages + 1;
    }
}
