package com.example.ecommerce.services;

import com.example.ecommerce.dao.CategoryDao;
import com.example.ecommerce.models.Category;
import com.example.ecommerce.models.Product;
import com.example.ecommerce.pagination.Page;

import java.util.List;

public class CategoryService implements Service<CategoryDao, Category> {
    private CategoryDao dao;

    public CategoryService() {
        dao = new CategoryDao();
    }

    public Page<Category> getPage(int pageNo, int size) {
        Page<Category> page = new Page<>();
        page.setPageNo(pageNo);
        page.setSize(size);
        page.setRecords(dao.getAll((pageNo - 1) * size, size));
        page.setTotal(dao.count());
        return page;
    }

    public void fetchCategoriesByProduct(Product p) {
        dao.fetchCategoriesByProduct(p);
    }

    @Override
    public Category get(int id) {
        return dao.get(id);
    }

    @Override
    public List<Category> getAll() {
        return dao.getAll();
    }

    @Override
    public void save(Category category) {
        dao.save(category);
    }

    @Override
    public void update(Category category) {
        dao.update(category);
    }

    @Override
    public void delete(Category category) {
        dao.delete(category);
    }
}
