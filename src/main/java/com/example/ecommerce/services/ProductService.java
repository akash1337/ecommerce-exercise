package com.example.ecommerce.services;


import com.example.ecommerce.dao.ProductDao;
import com.example.ecommerce.models.Product;
import com.example.ecommerce.pagination.Page;

import java.util.List;

public class ProductService implements Service<ProductDao, Product> {
    private ProductDao dao;

    public ProductService() {
        dao = new ProductDao();
    }

    public Page<Product> getPage(int pageNo, int size) {
        Page<Product> page = new Page<>();
        page.setPageNo(pageNo);
        page.setSize(size);
        page.setRecords(dao.getAll((pageNo - 1) * size, size));
        page.setTotal(dao.count());
        return page;
    }

    @Override
    public Product get(int id) {
        return dao.get(id);
    }

    @Override
    public List<Product> getAll() {
        return dao.getAll();
    }

    @Override
    public void save(Product product) {
        dao.save(product);
    }

    @Override
    public void update(Product product) {
        dao.update(product);
    }

    @Override
    public void delete(Product product) {
        dao.delete(product);
    }
}
